# Copyright (c) 2021-2024 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//foundation/ability/ability_runtime/ability_runtime.gni")

abilityms_files = [
  "src/ability_app_state_observer.cpp",
  "src/ability_background_connection.cpp",
  "src/ability_connect_manager.cpp",
  "src/ability_debug_deal.cpp",
  "src/ability_event_handler.cpp",
  "src/disposed_observer.cpp",
  "src/ability_manager_service.cpp",
  "src/ability_manager_stub.cpp",
  "src/ability_manager_proxy.cpp",
  "src/ability_record.cpp",
  "src/ability_manager_collaborator_proxy.cpp",
  "src/ability_scheduler_stub.cpp",
  "src/ability_scheduler_proxy.cpp",
  "src/ability_token_stub.cpp",
  "src/app_scheduler.cpp",
  "src/app_exit_reason_helper.cpp",
  "src/assert_fault_callback_death_mgr.cpp",
  "src/assert_fault_proxy.cpp",
  "src/connection_record.cpp",
  "src/data_ability_caller_recipient.cpp",
  "src/data_ability_manager.cpp",
  "src/data_ability_record.cpp",
  "src/dialog_session_record.cpp",
  "src/lifecycle_deal.cpp",
  "src/ability_running_info.cpp",
  "src/ecological_rule/ability_ecological_rule_mgr_service_param.cpp",
  "src/ecological_rule/ability_ecological_rule_mgr_service.cpp",
  "src/extension_config.cpp",
  "src/extension_running_info.cpp",
  "src/caller_info.cpp",
  "src/sender_info.cpp",
  "src/sub_managers_helper.cpp",
  "src/wants_info.cpp",
  "src/want_sender_info.cpp",
  "src/pending_want_record.cpp",
  "src/want_receiver_proxy.cpp",
  "src/want_receiver_stub.cpp",
  "src/want_sender_proxy.cpp",
  "src/want_sender_stub.cpp",
  "src/pending_want_key.cpp",
  "src/pending_want_manager.cpp",
  "src/pending_want_common_event.cpp",
  "src/restart_app_manager.cpp",
  "src/ams_configuration_parameter.cpp",
  "src/insight_intent_utils.cpp",
  "src/insight_intent_profile.cpp",
  "src/interceptor/ability_interceptor_executer.cpp",
  "src/interceptor/ability_jump_interceptor.cpp",
  "src/interceptor/control_interceptor.cpp",
  "src/interceptor/crowd_test_interceptor.cpp",
  "src/interceptor/disposed_rule_interceptor.cpp",
  "src/interceptor/ecological_rule_interceptor.cpp",
  "src/interceptor/start_other_app_interceptor.cpp",
  "src/uri_utils.cpp",
  "src/window_focus_changed_listener.cpp",

  # start ability handler
  "src/start_ability_handler.cpp",
  "src/start_ability_handler/start_ability_sandbox_savefile.cpp",
  "src/start_ability_utils.cpp",

  # new ability manager service here
  "src/task_data_persistence_mgr.cpp",
  "src/extension_record_manager.cpp",
  "src/extension_record.cpp",
  "src/extension_record_factory.cpp",
  "src/ui_extension_record.cpp",
  "src/ui_extension_record_factory.cpp",
  "src/start_options.cpp",
  "src/user_callback_proxy.cpp",
  "src/user_callback_stub.cpp",
  "src/call_container.cpp",
  "src/call_record.cpp",
  "src/inner_mission_info.cpp",
  "src/mission.cpp",
  "src/mission_data_storage.cpp",
  "src/mission_info_mgr.cpp",
  "src/mission_listener_controller.cpp",
  "src/mission_listener_proxy.cpp",
  "src/mission_listener_stub.cpp",
  "src/remote_mission_listener_proxy.cpp",
  "src/remote_mission_listener_stub.cpp",
  "src/mission_list_manager.cpp",
  "src/mission_list.cpp",
  "src/scene_board/status_bar_delegate_manager.cpp",
  "src/scene_board/ui_ability_lifecycle_manager.cpp",
  "src/open_link/open_link_options.cpp",

  #connection observer
  "src/connection_observer_controller.cpp",
  "src/connection_state_item.cpp",
  "src/connection_state_manager.cpp",
  "src/dlp_state_item.cpp",

  #multi user
  "src/user_controller.cpp",
  "src/user_event_handler.cpp",

  #free_install
  "src/atomic_service_status_callback_proxy.cpp",
  "src/atomic_service_status_callback_stub.cpp",
  "src/atomic_service_status_callback.cpp",
  "src/free_install_manager.cpp",
  "src/free_install_observer_manager.cpp",

  "src/background_task_observer.cpp",
  "src/resident_process_manager.cpp",

  "src/ability_bundle_event_callback.cpp",
  "src/ability_event_util.cpp",

  "src/app_exit_reason_data_manager.cpp",

  "src/ability_auto_startup_data_manager.cpp",
  "src/ability_auto_startup_service.cpp",
  "src/auto_startup_info.cpp",
  "src/insight_intent_execute_manager.cpp",
  "src/insight_intent_execute_result.cpp",

  "src/ability_manager_event_subscriber.cpp",
]

if (ability_runtime_graphics) {
  abilityms_files += [
    "${ability_runtime_services_path}/appdfr/src/application_anr_listener.cpp",
    "src/implicit_start_processor.cpp",
    "src/system_dialog_scheduler.cpp",
    "src/ability_first_frame_state_observer_manager.cpp",
  ]
}

if (efficiency_manager) {
  abilityms_files += [ "src/process_frozen_state_observer.cpp" ]
}
